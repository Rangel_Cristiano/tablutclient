package tablutclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
 
public class TablutClient {
    private static BufferedWriter mBufferedWriter;
    private static BufferedReader mBufferedReader;
    private static TablutTableView mTablutTable;
    private static Socket mSocket;
 
    public static void main(String args[]) {  
        mTablutTable = new TablutTableView(); 
        
        try {
            mSocket = new Socket("localhost", 1234);                        
            mBufferedWriter = new BufferedWriter(new OutputStreamWriter(mSocket.getOutputStream()));            
            mBufferedReader = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));                                   
            
            Integer[][] tableValues = new Integer[9][9];
            for (Integer line = 0; line < 9; line++) {
                for (Integer column = 0; column < 9; column++) {
                    tableValues[line][column] = mBufferedReader.read();
                }
            }    
            mTablutTable.updateTableView(tableValues); 
                   
            mTablutTable.setPlayer(mBufferedReader.read());            
            System.out.println("Player: " + mTablutTable.getPlayer()); 
            
            mTablutTable.updateToolBar(0, 0);
            
            if (mTablutTable.getPlayer() == 1) {
                mTablutTable.showAlertDialog("Sua Vez De Jogar !!");
            }
                      
            mTablutTable.setTurn(mBufferedReader.read());            
            
            if (mTablutTable.getPlayer() != mTablutTable.getTurn()) synchronizeUpdates();                            

        } catch (Exception exception){}
    }   
    
    public static void sendMove(Integer toLine, Integer toColumn, Integer fromLine, Integer fromColumn) {
        try {       
            mBufferedWriter.write(fromLine);       
            mBufferedWriter.write(fromColumn);
            mBufferedWriter.write(toLine);
            mBufferedWriter.write(toColumn);
            mBufferedWriter.flush();                     
         } catch (IOException ex) {
            Logger.getLogger(TablutClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void synchronizeUpdates() throws IOException {
        int capturedBlackPices = mBufferedReader.read();
        int capturedWhitePices = mBufferedReader.read();
      
        mTablutTable.updateToolBar(capturedBlackPices, capturedWhitePices);
        
        Integer[][] tableValues = new Integer[9][9];
        for (Integer line = 0; line < 9; line++) {
            for (Integer column = 0; column < 9; column++) {
                tableValues[line][column] = mBufferedReader.read();
            }
        }  
        mTablutTable.updateTableView(tableValues);            
        
        int playerWinner = mBufferedReader.read();
        if (playerWinner == 2) {
            mTablutTable.showAlertDialog("As Peças BRANCAS Ganharam");
            mBufferedReader.close();
            mBufferedWriter.close();
            mSocket.close();
            mTablutTable.exit();
        } else if (playerWinner == 1){           
            mTablutTable.showAlertDialog("As Peças PRETAS Ganharam");
            mBufferedReader.close();
            mBufferedWriter.close();
            mSocket.close();
            mTablutTable.exit();
        } else {                
            mTablutTable.setTurn(mBufferedReader.read());       
            if (mTablutTable.getPlayer() != mTablutTable.getTurn()) {
                synchronizeUpdates();
            } else {
               mTablutTable.showAlertDialog(" Sua Vez De Jogar !!! ");
            }   
        }
    }
}
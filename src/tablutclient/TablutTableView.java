package tablutclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class TablutTableView implements ActionListener {   
    private final JPanel mPanel = new JPanel(new BorderLayout(3, 3));
    private JButton[][] mTablutBoardButtons = new JButton[9][9];
    private int mPlayer;
    private JFrame mFrame;
    private Integer mFromLine, mFromColumn, mToLine, mToColumn;
    private ImageIcon mKingIcon, mPawnBlackIcon, mPawnWhiteIcon;
    private int mTurn = 1;
    private JDialog mDialog;

    public TablutTableView() {
        Runnable r = new Runnable() {
            @Override public void run() {
                initializeBoard();

                mFrame = new JFrame("Tablut");
                mFrame.add(mPanel);
                mFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                mFrame.pack();
                mFrame.setMinimumSize(new Dimension(600, 650));
                mFrame.setVisible(true);
            }
        };
        SwingUtilities.invokeLater(r);
    }

    private void initializeBoard() {
        final JPanel tablutBoardPanel;
        JToolBar tools = new JToolBar();      

        mPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        tools.setFloatable(false);
        mPanel.add(tools, BorderLayout.PAGE_START);
               
        tablutBoardPanel = new JPanel(new GridLayout(0, 9));
        tablutBoardPanel.setBorder(new LineBorder(Color.BLACK));
        mPanel.add(tablutBoardPanel);

        final Insets buttonMargin = new Insets(0, 0, 0, 0);
        for (Integer line = 0; line < 9; line++) {
            for (Integer column = 0; column < 9; column++) {
                JButton button = new JButton();
                button.setMargin(buttonMargin);
                button.setBackground(Color.PINK);
                
                mTablutBoardButtons[line][column] = button;               
                mTablutBoardButtons[line][column].addActionListener(this);
                
                tablutBoardPanel.add(mTablutBoardButtons[line][column]);              
            }
        }      
    }

    @Override public void actionPerformed(ActionEvent event) {
        if (mTurn == mPlayer) {
            mToLine = null;
            mToColumn = null;

            for (Integer line = 0; line < 9; line++) {
                for (Integer column = 0; column < 9; column++) {
                    if (event.getSource() == mTablutBoardButtons[line][column] && mTablutBoardButtons[line][column].getBackground() != Color.green) {
                        if (mFromLine == null && mFromLine == null && ((mTablutBoardButtons[line][column].getIcon() == mPawnBlackIcon && mPlayer == 1)
                                || (mTablutBoardButtons[line][column].getIcon() == mPawnWhiteIcon && mPlayer == 2) 
                                || (mPlayer == 2 && mTablutBoardButtons[line][column].getIcon() == mKingIcon))) {                            
                            mFromLine = line;
                            mFromColumn = column;
                        } else {
                           resetColorBoard();
                           
                           mFromLine = null;
                           mFromColumn = null;
                        }
                        break;
                    } else if (event.getSource() == mTablutBoardButtons[line][column] && mTablutBoardButtons[line][column].getBackground() == Color.green) {
                        mToLine = line;
                        mToColumn = column;
                        break;
                    }
                }
            }

            if (mFromLine != null || mFromColumn != null) {
                if (mToLine == null && mToColumn == null) {
                    showPossibleMoves(mFromLine, mFromColumn);
                } else {                                                   
                    Thread thread = new Thread(){
                        @Override public void run(){
                          TablutClient.sendMove(mFromLine, mFromColumn, mToLine, mToColumn); 
                            try {
                                TablutClient.synchronizeUpdates();
                            } catch (IOException ex) {
                                Logger.getLogger(TablutTableView.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                      };
                      thread.start();
                      
                      resetColorBoard();
                }
            } else {
                resetColorBoard(); 
                
                mFromLine = null;
                mFromColumn = null;
            }    
        } else {
          showAlertDialog("Aguarde seu Adversário ... ");
        }
    }
    
    public void updateTableView(Integer[][] tablutBoardValue) {
        mPawnBlackIcon = new ImageIcon(getClass().getResource("/pawn_black.png"));
        mPawnWhiteIcon = new ImageIcon(getClass().getResource("/pawn_white.png"));
        mKingIcon = new ImageIcon(getClass().getResource("/king.png"));
        
        for (Integer line = 0; line < 9; line++) {
            for (Integer column = 0; column < 9; column++) {
                if (tablutBoardValue[line][column] == 1) {
                    mTablutBoardButtons[line][column].setIcon(mPawnBlackIcon);
                } else if (tablutBoardValue[line][column] == 2) {
                    mTablutBoardButtons[line][column].setIcon(mPawnWhiteIcon);
                } else if (tablutBoardValue[line][column] == 3) {
                    mTablutBoardButtons[line][column].setIcon(mKingIcon);
                } else {
                    mTablutBoardButtons[line][column].setIcon(null);
                }               
            }
        }
        
          if (tablutBoardValue[4][4] == 0) {
            mTablutBoardButtons[4][4].setBackground(Color.red);
        }            
    }  

    public void resetColorBoard() {
        for (Integer line = 0; line < 9; line++) {
            for (Integer column = 0; column < 9; column++) {
                if (mTablutBoardButtons[line][column].getBackground() == Color.green) {
                    mTablutBoardButtons[line][column].setBackground(Color.pink);
                }
            }
        }             
    }

    public void showPossibleMoves(int sourceLine, int sourceColumn) {
        for (Integer line = mFromLine + 1; line < 9; line++) {
            if (mTablutBoardButtons[line][mFromColumn].getIcon() == null) {
                mTablutBoardButtons[line][mFromColumn].setBackground(Color.green);
            } else {
                break;
            }
        }

        for (Integer line = mFromLine - 1; line >= 0; line--) {
            if (mTablutBoardButtons[line][mFromColumn].getIcon() == null) {
                mTablutBoardButtons[line][mFromColumn].setBackground(Color.green);
            } else {
                break;
            }
        }

        for (Integer column = mFromColumn + 1; column < 9; column++) {
            if (mTablutBoardButtons[mFromLine][column].getIcon() == null) {
                mTablutBoardButtons[mFromLine][column].setBackground(Color.green);
            } else {
                break;
            }
        }

        for (Integer column = mFromColumn - 1; column >= 0; column--) {
            if (mTablutBoardButtons[mFromLine][column].getIcon() == null) {
                mTablutBoardButtons[mFromLine][column].setBackground(Color.green);
            } else {
                break;
            }
        }

        if (mTablutBoardButtons[4][4].getIcon() == null) {
            mTablutBoardButtons[4][4].setBackground(Color.red);
        }
    }
    
    public void showAlertDialog(String message) {          
        JOptionPane messagePane = new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE);
        if (mDialog != null)  mDialog.setVisible(false);
        mDialog = messagePane.createDialog(mFrame, "AVISO");
        mDialog.setVisible(true);
    }
    
    public void updateToolBar(int capturedBlackPices, int capturedWhitePices) {
        final JToolBar tools = new JToolBar();
        mPanel.add(tools, BorderLayout.PAGE_START);
        
         if (mPlayer == 1) {
         tools.add(new JLabel("Jogador " + mPlayer + " - Peças PRETAS"));
        } else {
         tools.add(new JLabel("Jogador " + mPlayer + " - Peças BRANCAS"));  
        }
        
        tools.addSeparator();
        tools.addSeparator();
        tools.add(new JLabel("PRETAS Capturadas: " + capturedBlackPices));
        tools.addSeparator();
        tools.add(new JLabel("BRANCAS Capturadas " + capturedWhitePices));       
    }
    
    public void setPlayer(int player) {
        mPlayer = player;      
    }
    
    public void exit() {
        mFrame.dispose();
    }
    
    public int getPlayer() {
        return mPlayer;
    }
     
    public int getTurn() {
        return mTurn;
    }

    public void setTurn(int turn) {
        mTurn = turn;
    }
}
